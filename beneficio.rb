

#
# Hallar todas las combinaciones posibles de los objetos
#

def hallar_combinaciones(objetos)
  combinaciones = []
  1.upto(objetos.size) do |index|
    # Con combinaciones sin repetir
    # combinaciones = combinaciones +objetos.combination(index).to_a
    # Con combinaciones repetidas
    combinaciones = combinaciones + objetos.repeated_combination(index).to_a
  end
  combinaciones
end

#
# Filtrar combinaciones cuyo peso sea menor o igual a 15
#

def filtrar_combinaciones(combinaciones, capacidad_maxima)
  combinaciones_filtradas = [] #su peso es menor o igual a capacidad_maxima

  combinaciones.each do |combinacion|
    suma_peso = 0  
    combinacion.each do |objeto|
      suma_peso += objeto[:peso]
    end  
    if suma_peso <= capacidad_maxima
      combinaciones_filtradas << combinacion
    end
  end
  combinaciones_filtradas
end
#p combinaciones_filtradas

#
# Hallar el beneficio de cada combinacion filtrada
#

def hallar_beneficio(combinaciones_filtradas)
  combinaciones_filtradas.map do |combinacion|
    suma_beneficio = 0
    combinacion.each do |objeto|
      suma_beneficio += objeto[:beneficio]
    end
    combinacion << {:suma_beneficio => suma_beneficio}
  end
  combinaciones_filtradas
end

#
# Ordenar las combinaciones filtradas por su beneficio total
#

def ordenar_combinaciones(combinaciones_filtradas)
  combinaciones_filtradas.sort_by! do |combinacion| 
    combinacion.last[:suma_beneficio].to_i
  end
  combinaciones_filtradas
end

#
# Halla el la combinacion con el maximo beneficio
#

def maximo_beneficio(objetos, capacidad_maxima)
  a = hallar_combinaciones(objetos)
  b = filtrar_combinaciones(a, capacidad_maxima)
  c = hallar_beneficio(b)
  d = ordenar_combinaciones(c)
  elegido =  d.last
  puts "---------------------------------------------------------------------"
  puts "EL MAXIMO BENEFICIO OBTENIDO FUE DE #{elegido.last[:suma_beneficio]} |"
  puts "CON LA COMBINACION DE OBJETOS #{elegido[0..-1].each{ |a| p a}}                      |"
  puts "---------------------------------------------------------------------"
end



#
# Programa principal: Hallar la combinacion con el maximo beneficio dados los 
# objetos con peso y beneficio,  y la capacidad maxima de peso.
# Se hace uso de las funciones arriba declaradas.
#


#
# Problema 1
#
capacidad_maxima = 15

objetos = [
{ :peso => 3, :beneficio => 12 },
{ :peso => 7, :beneficio => 3 },
{ :peso => 4, :beneficio => 7 },
{ :peso => 2, :beneficio => 4 },
{ :peso => 1, :beneficio => 3 },
{ :peso => 6, :beneficio => 8 },
]

maximo_beneficio(objetos, capacidad_maxima)

#
# Problema 2
#
 
capacidad_maxima = 255

#=begin
objetos = [
{ :peso => 51, :beneficio => 204 },
{ :peso => 119, :beneficio => 51 },
{ :peso => 68, :beneficio => 119 },
{ :peso => 34, :beneficio => 68 },
{ :peso => 17, :beneficio => 51 },
{ :peso => 102, :beneficio => 136 },
]
#=end

maximo_beneficio(objetos, capacidad_maxima)

#
# Problema 3
#
 
capacidad_maxima = 1000

#=begin
objetos = [
{ :peso => 130, :beneficio => 120 },
{ :peso => 570, :beneficio => 300 },
{ :peso => 140, :beneficio => 570 },
{ :peso => 200, :beneficio => 423 },
{ :peso => 360, :beneficio => 300 },
{ :peso => 400, :beneficio => 800 },
]
#=end

maximo_beneficio(objetos, capacidad_maxima)

